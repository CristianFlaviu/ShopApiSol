﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ShopApi.Database.Data
{
    public static class DatabaseHelper
    {
        public static void configureDB(this IApplicationBuilder app)

        {

            try
            {
                using var serviceScope = app.ApplicationServices

                    .GetRequiredService<IServiceScopeFactory>()

                    .CreateScope();

                using var context = serviceScope.ServiceProvider.GetService<DataContext>();

                context.Database.Migrate();
            }
            catch (Exception ex)
            {


            }

        }
    }
}
